const TEXT_NODE_TYPE = 3;

export const createVNode = (tagName, props = {}, children = []) => {
  return {
    tagName,
    props,
    children,
  };
};

export const createDOMNode = (vNode) => {
  // Обработаем текстовые ноды
  if (typeof vNode === 'string') {
    return document.createTextNode(vNode);
  }

  const { tagName, props, children } = vNode;

  const node = document.createElement(tagName);

  patchProps(node, {}, props);

  // Рекурсия не лучший вариант для обхода DOM-узлов, тк вложенность может быть очень большой.
  // Здесь целесообразно использовать стек.
  // Оставим рекурсию для сохранения простоты реализации.
  children.forEach((child) => {
    node.appendChild(createDOMNode(child));
  });

  return node;
};

export const mount = (node, target) => {
  // Есть несколько способов: appendChild, innerHTML и так далле.
  // Мы просто заменим target на сгенерированную ноду.
  // Выбранный способ не подойдет, если с сервера уже приходит html, тк мы лишний раз польностью перерисуем DOM.
  target.replaceWith(node);
  return node;
};

export const patchNode = (node, vNode, nextVNode) => {
  if (nextVNode === undefined) {
    node.remove();
    return;
  }

  if (typeof vNode === 'string' || typeof nextVNode === 'string') {
    if (vNode !== nextVNode) {
      const nextNode = createDOMNode(nextVNode);
      node.replaceWith(nextNode);
      return nextNode;
    }

    return node;
  }

  patchProps(node, vNode.props, nextVNode.props);
  patchChildren(node, vNode.children, nextVNode.children);

  return node;
};

const patchProps = (node, props, nextProps) => {
  const mergedProps = { ...props, ...nextProps };

  Object.keys(mergedProps).forEach((key) => {
    if (props[key] !== nextProps[key]) {
      patchProp(node, key, props[key], nextProps[key]);
    }
  });
};

const patchProp = (node, key, value, nextValue) => {
  if (key.startsWith('on')) {
    const eventName = key.slice(2);
    node[eventName] = nextValue;

    if (!nextValue) {
      node.removeEventListener(eventName, listener);
    } else if (!value) {
      node.addEventListener(eventName, listener);
    }

    return;
  }

  if (nextValue === null || nextValue === false) {
    node.removeAttribute(key);
    return;
  }

  node.setAttribute(key, nextValue);
};

const patchChildren = (parent, vChildren, nextVChildren) => {
  parent.childNodes.forEach((childNode, idx) => {
    patchNode(childNode, vChildren[idx], nextVChildren[idx]);
  });

  nextVChildren.slice(vChildren.length).forEach((vChild) => {
    parent.appendChild(createDOMNode(vChild));
  });
};

export const patch = (nextVNode, node) => {
  const vNode = node.v || recycleNode(node);
  node = patchNode(node, vNode, nextVNode);
  node.v = nextVNode;
  return node;
};

const recycleNode = node => {
  if(node.nodeType === TEXT_NODE_TYPE) {
    return node.nodeValue;
  }

  const tagName = node.nodeName.toLowerCase();
  const children = [].map.call(node.childNodes, recycleNode);

  return createVNode(tagName, {}, children);
};

function listener(event) {
  return this[event.type](event);
}