# Vue.js Single File Component Factory
Одним из способов инжектировать зависимости является использование функций-фабрик, которые принимают зависимости как параметры. По [спецификации SFC](https://vue-loader.vuejs.org/spec.html) дефолтный экспорт должен быть объетом опций, соответственно, не получится просто экспортировать функцию.

## Чем хороши factory functions
Рассмотрим пример:
```vue
<template>
  <button @click="load">
    Load articles
  </button>
</template>

<script>
import article from '../services/article';

export default {
  name: 'LoadArticles',
  methods: {
    async load() {
      const articles = await article.list();
      this.$emit('new-articles', articles);
    },
  },
};
</script>
```
Это типичный ```.vue`` файл. Мы испортируем ```article```, что увеличивает связанность компонентов и усложняет использование и тестирование. Рассмотрим другую реализацию функциональности:
```js
export const loadArticlesFactory = ({ article }) => ({
  name: 'LoadArticles',
  template: '<button @click="load">Load articles</button>',
  methods: {
    async load() {
      const articles = await article.list();
      this.$emit('new-articles', articles);
    },
  },
});
```
Продолжив, мы можем сделать наш компонент более гибким:
```js
export const loadEntitiesFactory = ({ loadEntities }) => ({
  name: 'LoadEntities',
  template: '<button @click="load"><slot/></button>',
  methods: {
    async load() {
      const entities = await loadEntities();
      this.$emit('new-entities', entities);
    },
  },
});
```
```vue
<template>
  <div>
    <LoadArticles>
      Load articles
    </LoadArticles>
    <LoadComments>
      Load comments
    </LoadComments>
  </div>
</template>

<script>
import article from '../services/article';
import comment from '../services/comment';

import { loadEntitiesFactory } from './LoadEntities';

export default {
  name: 'StuffLoader',
  components: {
    LoadArticles: loadEntitiesFactory({
      loadEntities: article.list,
    }),
    LoadComments: loadEntitiesFactory({
      loadEntities: comment.list,
    }),
  },
};
</script>
```
Теперь ```LoadEntities``` легко переиспользовать, а ```StuffLoader``` выступает в роли [контейнера](https://markus.oberlehner.net/blog/advanced-vue-component-composition-with-container-components/).


## ```export default``` - возвращаем функцию
Перейдем в примеру:
```vue
<template>
  <button @click="load">
    <slot/>
  </button>
</template>

<script>
import { makeFactory, makeGuard } from '../utils/sfc-factory';

// Because in JavaScript objects are passed by reference,
// we can reuse this options object for creating new
// components with the factory function. And because we also
// use this options object as the default export, the Vue
// Loader extends it with a render function which is generated
// of the markup in the <template> section.
const options = {};

export const loadEntitiesFactory = makeFactory(options, ({ loadEntities }) => ({
  name: 'LoadEntities',
  methods: {
    async load() {
      const entities = await loadEntities();
      this.$emit('new-entities', entities);
    },
  },
}));

// It is important to export the (guarded) options
// object as the default export of the component.
export default makeGuard(options);
</script>
```

```makefactory()```:
```js
// src/utils/sfc-factory.js
export function makeFactory(options, componentFactory) {
  return (...params) => ({
    ...options,
    // Overwrite the `beforeCreate()` hook added by `makeGuard()`.
    beforeCreate() {},
    ...componentFactory(...params),
  });
}
```
Наша функция ```makeFactory()``` принимает объект ```options```, который используется в ```export default```. Из функции возвращается новая функция, которая мержит свойства объекта ```options``` (наиболее примечательна ```render()```-функция, которую добавляет ```vue-loader```) с опциями, полученными из ```componentFactory()```.

Функция ```makeGuard()``` не является обязательной, но позволяет прояснить другим разработчикам, что в конкретных компонентах стоит использовать ```factory function```, а не ```export default```:

```js
// src/utils/sfc-factory.js
export function makeGuard(options) {
  return Object.assign(options, {
    beforeCreate() {
      throw new Error('Do not use the default export but use the factory function instead!');
    },
  });
}
```