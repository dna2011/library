# April 2021

## Fundamentals

### IDL - interface description language. Special language for objects' interfaces description.
In frontend we can meet IDL when we work with HTML elements' attributes, attributes can be _content_ or _IDL_. Content attributes are attributes that we set in HTML code, we can reach them from JS (`el.getAttribute()`, `el.setAttribute()`). IDL attributes are DOM elements properties (`element.foo`). Most of the time IDL attributes will return value that actually been used. But there are edge cases, see [spec](https://html.spec.whatwg.org/multipage/common-dom-interfaces.html#reflecting-content-attributes-in-idl-attributes).

### Finite state machine - mathematical abstraction using in algorithm deisgn. This machine reads entering data squence. If machine read a signal, it toggles to a new state. New state depends on current state and calculates inside of machine.

### L-system
L-system consists of characters alphabet, set of rules, initial string (axiom) and mapping mechanism (result string -> geometry structure).

**Example: Pythagoras tree**
 - variables: `1, 0`
 - constants: `[, ]`
 - axiom: `0`
 - rules:
   - `0 -> 1[0]0`
   - `1 -> 11`

1. axiom:	`0`
2. 1st recursion:	`1[0]0`
3. 2nd recursion:	`11[1[0]0]1[0]0`
4. 3rd recursion:	`1111[11[1[0]0]1[0]0]11[1[0]0]1[0]0`

**Mapping**
Mappinng can be done using set of turtle [graphics rules](https://ru.wikipedia.org/wiki/%D0%A7%D0%B5%D1%80%D0%B5%D0%BF%D0%B0%D1%88%D1%8C%D1%8F_%D0%B3%D1%80%D0%B0%D1%84%D0%B8%D0%BA%D0%B0).

- 0: draw line ending with list
- 1: draw line
- [: put current coordinates and angle in stack, turn left 45deg
- ]: get coordinates and angle from stack, turn right 45deg

